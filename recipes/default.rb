#
# Copyright 2012, David P. Kleinschmidt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

include_recipe 'ingredients'

template '/etc/apt/preferences' do
  mode      0644
  source    'preferences.erb'
  variables apt_preferences: debian.apt_preferences
end

debian.sources.each do |source|
  unless source.key.nil?
    execute "apt-key:#{source.url}" do
      command "curl -Lso - #{source.key} | apt-key add -"
    end
  end
end

template '/etc/apt/sources.list' do
  mode      0644
  source    'sources.list.erb'
  variables mirror: debian.mirror, sources: debian.sources

  notifies :run, 'execute[apt-get:update]', :immediately
end

execute 'apt-get:update' do
  command 'apt-get update'

  action :nothing
end

debian.packages.each do |p|
  package p
end
